import keyboard as kb

rez_amount = 0
while True:
    if kb.is_pressed('9'):
        rez_amount = rez_amount + 1
        print(
            "'Resurrect' was used on Valie {} times".format(
                rez_amount
            )
        )
        kb.wait('9')